import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:geocoding/geocoding.dart';
import 'package:geolocator/geolocator.dart';
import 'package:shared_preferences/shared_preferences.dart';

/* Defining the shared preferences and firebase authentication here so need to be called
  again and again in every dart file we need
*/


SharedPreferences? sharedPreferences;
FirebaseAuth firebaseAuth = FirebaseAuth.instance;


// Defining the global variables of position and location to be called in multiple files

Position? position;
List<Placemark>? placeMarks;
String newCompleteAddress ="";