import 'package:anydelivery_riders/authentication/auth_screen.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

import '../global/global.dart';
import '../mainScreens/homescreen.dart';
import '../widgets/custom_text_field.dart';
import '../widgets/error_dialog.dart';
import '../widgets/loading_dialog.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen>
{
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>(); // Defining the form to enter the user information for login
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();


  formValidation () // Checking if the user entered the correct credentials to login
  {
    if(emailController.text.isNotEmpty && passwordController.text.isNotEmpty)
      {
        //login
        loginNow();

      }
    else
      {
        showDialog(context: context, builder: (c){
          return ErrorDialog(message: "Email or Password Cannot be Blank",);
        });
      }
  }

  loginNow() async // function to authenticate user in firebase and get their credentials
  {
    showDialog(context: context, builder: (c){
      return LoadingDialog(message: "Checking Credentials",);
    });

    User? currentUser;
    await firebaseAuth.signInWithEmailAndPassword(
      email: emailController.text.trim(),
      password: passwordController.text.trim(),
    ).then((auth) {
      currentUser = auth.user!;
    }).catchError((error){
      Navigator.pop(context);
      showDialog(context: context, builder: (c){
        return ErrorDialog(message: error.toString(),);
      });

    });
    if(currentUser!=null)
    {
      readDataAndSetDataLocally(currentUser!).then((value){
        Navigator.pop(context);

      });
    }
  }

  Future readDataAndSetDataLocally (User currentUser) async
  {
    await FirebaseFirestore.instance.collection("riders").doc(currentUser.uid).get()
        .then((snapshot) async
    {
      // to Check whether the email and password is valid for riders and not other app login details

      if (snapshot!=null || snapshot.exists)
        {
          /*
      The above snapshot retrieves from the collection of riders a current snapshot or
      values stored in the doc of the current Userid (UID)
      We can then retrieve their data to save in shared preferences by calling the data
      in that snapshot
       */
          await sharedPreferences!.setString("uid", currentUser.uid);
          await sharedPreferences!.setString("name", snapshot.data()!["Name"]);
          await sharedPreferences!.setString("email", snapshot.data()!["riderEmail"]);
          await sharedPreferences!.setString("phone", snapshot.data()!["Phone"]);
          await sharedPreferences!.setString("photoUrl", snapshot.data()!["riderAvatarUrl"]);
          Navigator.push(context, MaterialPageRoute(builder: (c)=> const HomeScreen()));


        }
      else
        {
          showDialog(context: context, builder: (c){
            return ErrorDialog(message: "Login Details do not exist",);
          });
          firebaseAuth.signOut();
          Navigator.push(context, MaterialPageRoute(builder: (c)=> const AuthScreen()));
        }


    });

  }


  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        mainAxisSize: MainAxisSize.max,
        children: [
          Container(
            alignment: Alignment.bottomCenter,
            child: Padding(
              padding: EdgeInsets.all(15),
              child: Image.asset(
                  "images/signup.png",
                   height: 270,),
            ),
          ),
          Form(
            key: _formKey,
            child: Column(
              children: [
                CustomTextField(
                  data: Icons.email,
                  controller: emailController,
                  hintText: "Email",
                  isObscure: false,
                ),// EmailTextField
                CustomTextField(
                  data: Icons.lock_clock,
                  controller: passwordController,
                  hintText: "Enter Password",
                  isObscure: true,
                ), // PasswordField

              ],
            ),
          ), // Sign IN Form
          const SizedBox(height: 30,),
          ElevatedButton(
            child: const Text(
              "Login",
              style:TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
            ),
            style: ElevatedButton.styleFrom(
              primary: Colors.cyan, // color of Signup Button
              padding: EdgeInsets.symmetric(horizontal: 50, vertical: 20),
            ),
            onPressed: (){
              formValidation();
            },
          ),

        ],

      ),
    );
  }
}
