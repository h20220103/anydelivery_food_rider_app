import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:geocoding/geocoding.dart';
import 'package:geolocator/geolocator.dart';
import 'package:image_picker/image_picker.dart';
import 'package:flutter/services.dart';
import 'package:firebase_storage/firebase_storage.dart' as fstorage;
import 'package:shared_preferences/shared_preferences.dart';

import '../global/global.dart';
import '../mainScreens/homescreen.dart';
import '../widgets/custom_text_field.dart';
import '../widgets/error_dialog.dart';
import '../widgets/loading_dialog.dart';

class RegisterScreen extends StatefulWidget {
  const RegisterScreen({Key? key}) : super(key: key);

  @override
  State<RegisterScreen> createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>(); // Defining the form to enter the user information
  TextEditingController nameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController confirmPasswordController = TextEditingController();
  TextEditingController phoneController = TextEditingController();
  TextEditingController locationController = TextEditingController();

  XFile? imageXfile;
  final ImagePicker _picker = ImagePicker();

  Position? position;
  List<Placemark>? placeMarks;
  String riderImageurl = "";
  String newCompleteAddress ="";

  Future<void> _getImage() async{
    imageXfile=await _picker.pickImage(source: ImageSource.gallery); // Function to allow the select an image from the gallery // Await as the function is asynchronous

    setState(() {
      imageXfile; // to ensure we get the image from the gallery and display in the app
    });
  } // Image Getter function from gallery

  getCurrentLocation() async // Position Function to get the current location of the shop
  {
    LocationPermission permission;
    permission = await Geolocator.checkPermission(); // Need to check permission for the Geolocation before requesting for location
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.deniedForever) {
        return Future.error('Location Not Available');
      }
    }


    Position newPosition = await Geolocator.getCurrentPosition(
      desiredAccuracy: LocationAccuracy.bestForNavigation,
    );
    position = newPosition;
    placeMarks =  await placemarkFromCoordinates(
      position!.latitude,
      position!.longitude,
    ); // Google returns multiple in dex locations from this function and we need the 0th index one

    Placemark pMark = placeMarks![0]; // Get the  best location from in latitude and longitude from the array of locations returned by the geolocator function call

    // Below code converts the latitude and longitiude that is returned from geolocator app to a street address which more easily comprehensible by humans
    newCompleteAddress = '${pMark.subThoroughfare} ${pMark.thoroughfare}, ${pMark.subLocality} ,${pMark.locality}, ${pMark.subAdministrativeArea} , ${pMark.administrativeArea}, ${pMark.postalCode}, ${pMark.country}'; // subThoroughfare, Thoroughfare postal code and locality etc  converts the address. Check documentation of dart repositary for more info
    locationController.text = newCompleteAddress;




  }

  Future<void> FormValidation () async
  {
    if (imageXfile == null) {
      showDialog(context: context,
          builder: (c)
      {
        return ErrorDialog(
          message: "Please select an Image for Shop",
        );
      });
    }
    else {
      if (passwordController.text == confirmPasswordController.text )
        {
          if (passwordController.text.isNotEmpty && emailController.text.isNotEmpty && nameController.text.isNotEmpty && phoneController.text.isNotEmpty && locationController.text.isNotEmpty)
          {
            //starting to upload the image the data and picture to database
            showDialog(
              context: context,
              builder: (c)
                {
                  return LoadingDialog(
                    message: "Registering new account",
                  );
                }

            );

            String filename = DateTime.now().millisecondsSinceEpoch.toString(); // Using the current time in millisecond to store unique filenames
            // Storing the image in the Firebase Storage
            fstorage.Reference reference  = fstorage.FirebaseStorage.instance.ref().child("riders").child(filename);
            fstorage.UploadTask uploadTask = reference.putFile(File(imageXfile!.path));
            fstorage.TaskSnapshot taskSnapshot = await uploadTask.whenComplete(() {});
            await taskSnapshot.ref.getDownloadURL().then((url){
              riderImageurl = url;

              //save info to firestore

              signUpandAuthenticate();

            });



          }
          else
            {
              showDialog(context: context,
                  builder: (c)
                  {
                    return ErrorDialog(
                      message: "Please enter the complete information for Registration",
                    );
                  });

            }

        }
      else {
        showDialog(context: context,
            builder: (c)
            {
              return ErrorDialog(
                message: "Passwords do not match",
              );
            });

      }
    }
  }

  // firebase authentication code
  void signUpandAuthenticate() async
  {
    User? currentuser;

    await firebaseAuth.createUserWithEmailAndPassword(
        email: emailController.text.trim(),
        password: passwordController.text.trim(),).then((auth)
    {
      currentuser = auth.user;
    }).catchError((error){
      Navigator.pop(context);
      showDialog(context: context,
          builder: (c)
          {
            return ErrorDialog(
              message: error.message.toString().trim(),
            );
          });


    });

    if(currentuser!=null)
      {
        // If current user is registered then the information for them is save to the firestore database by calling the saveDatatoFileStore function
        saveDatatoFileStore(currentuser!).then((value)
        {
          Navigator.pop(context); // Stopping the progressbar

          //Sending the User to their homescreen after successful Registration
          Route newRoute = MaterialPageRoute(builder: (c) => const HomeScreen());
          Navigator.pushReplacement(context, newRoute);
        });
      }

  }

  /*Firebase Storage saves files in a various collections with each collection containing
  docs as separate entries.
  on authentication the users get unique UIDS from firestore which are then used as primary key
  identifiers to save their information like name address , phone earning status etc....
  * */
  Future saveDatatoFileStore (User currentuser) async {
    FirebaseFirestore.instance.collection("riders").doc(currentuser.uid).set({
      "riderUID": currentuser.uid,
      "riderEmail": currentuser.email,
      "Name": nameController.text.trim(),
      "riderAvatarUrl": riderImageurl,
      "Phone": phoneController.text.trim(),
      "Address": newCompleteAddress,
      "status": "Approved",
      "earning": "0.0",
      "lat": position!.latitude,
      "long": position!.longitude,

    });

    // Also need to implement code to save them locally in order for easier access


    sharedPreferences =await SharedPreferences.getInstance(); // Retrieveing shared Preference from global.dart file in global directory

    await sharedPreferences!.setString("uid", currentuser.uid);
    await sharedPreferences!.setString("name", nameController.text.trim());
    await sharedPreferences!.setString("photoUrl", riderImageurl);
    await sharedPreferences!.setString("email", emailController.text.toString().trim());
    await sharedPreferences!.setString("phone", phoneController.text.trim());

  }


  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: [
            const SizedBox(height: 15,),
            InkWell(
              onTap:  ()
              {
                _getImage();
              },
              child: CircleAvatar(
                radius: MediaQuery.of(context).size.width * 0.20,
                backgroundColor: Colors.white,
                backgroundImage: imageXfile == null? null : FileImage(File(imageXfile!.path)),
                child: imageXfile == null ? Icon(Icons.add_photo_alternate_rounded,
                size: MediaQuery.of(context).size.width * 0.20,
                color: Colors.grey,) : null ,
              ),

            ), // Choosing Image for the Shop
            const SizedBox(height: 10,),
            Form(
              key: _formKey,
              child: Column(
                children: [
                  CustomTextField(
                    data: Icons.person,
                    controller: nameController,
                    hintText: "Name",
                    isObscure: false,
                  ), // NameTextField
                  CustomTextField(
                    data: Icons.email,
                    controller: emailController,
                    hintText: "Email",
                    isObscure: false,
                  ),// EmailTextField
                  CustomTextField(
                    data: Icons.lock_clock,
                    controller: passwordController,
                    hintText: "Enter a Strong Password",
                    isObscure: true,
                  ), // PasswordField
                  CustomTextField(
                    data: Icons.lock_clock,
                    controller: confirmPasswordController,
                    hintText: "ConfirmPassword",
                    isObscure: true,
                  ),//ConfirmPassword Text field
                  CustomTextField(
                    data: Icons.phone,
                    keyboardType: TextInputType.number,
                    controller: phoneController,
                    hintText: "enter Phone number",
                    isObscure: false,
                  ), //Phone Number
                  CustomTextField(
                    data: Icons.my_location_rounded,
                    controller: locationController,
                    hintText: "Current Address",
                    isObscure: false,
                    enabled: false,
                  ), //Location Field
                  Container(
                    width: 400, height: 40,
                    alignment: Alignment.center,
                    child: ElevatedButton.icon(
                      label: const Text( "Get Current Location",
                          style: TextStyle(color: Colors.white),
                      ),
                      icon: const Icon(
                        Icons.location_on,
                        color: Colors.white,
                      ),
                      onPressed: (){
                        getCurrentLocation();
                      },
                      style: ElevatedButton.styleFrom(
                        primary: Colors.amber,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(30),

                        )
                      ),



                    ),
                  )// Location Button
                ],
              )

            ),
            const SizedBox(height: 30,),
            ElevatedButton(
              child: const Text(
              "Sign Up",
                style:TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
              ),
              style: ElevatedButton.styleFrom(
                primary: Colors.purple, // color of Signup Button
                padding: EdgeInsets.symmetric(horizontal: 50, vertical: 20),
              ),
              onPressed: ()
              {
                FormValidation();


              },
            ),

            const SizedBox(height: 30,),

          ],
        ),
      )
    );
  }
}
