import 'package:anydelivery_riders/global/global.dart';
import 'package:anydelivery_riders/mainScreens/parcelDeliveringScreen.dart';
import 'package:anydelivery_riders/maps/maps.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class ParcelPickupScreen extends StatefulWidget {
  String? purchaserID;
  String? sellerID;
  String? getOrderID;
  String? purchaserAddress;
  double? purchaserLat;
  double? purchaserLng;

  ParcelPickupScreen({
    this.purchaserID,
    this.sellerID,
    this.getOrderID,
    this.purchaserAddress,
    this.purchaserLat,
    this.purchaserLng,
  });

  @override
  State<ParcelPickupScreen> createState() => _ParcelPickupScreenState();
}



class _ParcelPickupScreenState extends State<ParcelPickupScreen> {

  double? sellerLat,sellerLong;
  getSellerData() async
  {
    FirebaseFirestore.instance.collection("sellers").doc(widget!.sellerID).get().then((DocumentSnapshot)
    {
      sellerLat = DocumentSnapshot.data()!["lat"];
      sellerLong = DocumentSnapshot.data()!["long"];
    });
  }

  @override
  void initState() {

    super.initState();
    getSellerData();
  }

  confirmedParcelPicked(getOrderID, sellerID, purchaserID,purchaserAddress, purchaserLat, purchaserLong)
  {
    FirebaseFirestore.instance
        .collection("orders")
        .doc(getOrderID).update({
      "status": "delivering",
      "address": newCompleteAddress,
      "lat": position!.latitude,
      "lng": position!.longitude,
    });

    Navigator.push(context, MaterialPageRoute(builder: (c)=> ParcelDeliveringScreen(
      purchaserID: purchaserID,
      purchaserAddress: purchaserAddress,
      purchaserLat: purchaserLat,
      purchaserLong: purchaserLong,
      sellerID: sellerID,
      getOrderID: sellerID,
    )));

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image.asset(
            "images/confirm1.png",
            width: 350,

          ),
          SizedBox(height: 5,),
          GestureDetector(
            onTap: ()
            {
              //show location from rider current location to seller location
              MapsUtils.lauchMapFromSourceToDestination(position!.latitude, position!.longitude, sellerLat, sellerLong);
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Image.asset(
                  "images/restaurant.png",
                  width: 50,
                ),
                SizedBox(height: 5,),
                 Column(
                  children:  const [
                    SizedBox(height: 7,),
                    Text("Show Cafe/Resturant Location",
                    style: TextStyle(
                      fontSize: 18,
                      letterSpacing: 2,
                      fontFamily: "Signatra"
                    ),
                    ),
                  ],
                ),


              ],
            ),
          ),
          SizedBox(height: 15,),
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: Center(
              child: InkWell(
                onTap: ()
                {
                  // Confirmed that rider has picked up parcel from seller


                },
                child: Container(
                  decoration: const BoxDecoration(
                      gradient: LinearGradient(
                        colors: [
                          Colors.cyan,
                          Colors.amber,
                        ],
                        begin:  FractionalOffset(0.0, 0.0),
                        end:  FractionalOffset(1.0, 0.0),
                        stops: [0.0, 1.0],
                        tileMode: TileMode.clamp,
                      )
                  ),
                  width: MediaQuery.of(context).size.width - 70,
                  height: 50,
                  child: const Center(
                    child: Text(
                      "Order Has been Picked -- Confirmed",
                      style: TextStyle(color: Colors.white, fontSize: 15.0),
                    ),
                  ),
                ),
              ),
            ),
          ),


        ],
      ),
    );
  }
}
