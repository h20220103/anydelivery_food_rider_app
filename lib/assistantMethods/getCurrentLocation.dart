
import 'package:flutter/cupertino.dart';
import 'package:geocoding/geocoding.dart';
import 'package:geolocator/geolocator.dart';

import '../global/global.dart';

class UserLocation
{

  TextEditingController locationController = TextEditingController();

  getCurrentLocation() async // Position Function to get the current location of the shop
      {
    LocationPermission permission;
    permission = await Geolocator.checkPermission(); // Need to check permission for the Geolocation before requesting for location
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.deniedForever) {
        return Future.error('Location Not Available');
      }
    }


    Position newPosition = await Geolocator.getCurrentPosition(
      desiredAccuracy: LocationAccuracy.high,
    );
    position = newPosition;
    placeMarks =  await placemarkFromCoordinates(
      position!.latitude,
      position!.longitude,
    ); // Google returns multiple in dex locations from this function and we need the 0th index one

    Placemark pMark = placeMarks![0]; // Get the  best location from in latitude and longitude from the array of locations returned by the geolocator function call

    // Below code converts the latitude and longitiude that is returned from geolocator app to a street address which more easily comprehensible by humans
    newCompleteAddress = '${pMark.subThoroughfare} ${pMark.thoroughfare}, ${pMark.subLocality} ,${pMark.locality}, ${pMark.subAdministrativeArea} , ${pMark.administrativeArea}, ${pMark.postalCode}, ${pMark.country}'; // subThoroughfare, Thoroughfare postal code and locality etc  converts the address. Check documentation of dart repositary for more info
    locationController.text = newCompleteAddress;




  }

}
