import 'package:anydelivery_riders/assistantMethods/getCurrentLocation.dart';
import 'package:anydelivery_riders/global/global.dart';
import 'package:anydelivery_riders/mainScreens/homescreen.dart';
import 'package:anydelivery_riders/mainScreens/parcelPickupScreen.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

import '../models/address.dart';
import '../splashScreen/splash_screen.dart';





class ShipmentAddressDesign extends StatelessWidget
{
  final Address? model;
  final String? orderStatus;
  final String? orderID;
  final String? sellerID;
  final String? orderByUser;



  ShipmentAddressDesign({this.model, this.orderStatus,this.orderID,this.sellerID,this.orderByUser});

  // function to confirm delivery of order
  confirmedOrderShipment(BuildContext context, String getOrderID, String sellerID, String purchaserID) {
    // In the firebaseFirestore Order status is set to normal and no rider is assigned at first when user
    // places the order.
    // This function changes status to "on way to Pick it up from seller" and assign a Rider

    FirebaseFirestore.instance.collection("orders").
    doc(getOrderID).
    update({
      "riderUID" : sharedPreferences!.getString("uid"),
      "riderName": sharedPreferences!.getString("name"),
      "status": "En Route to Shop",
      "lat": position!.latitude,
      "lng": position!.longitude,
      "address": newCompleteAddress,

    });

    //Send Rider to Shipment Screen

    Navigator.push(context, MaterialPageRoute(builder: (c) => ParcelPickupScreen(
      purchaserID: purchaserID,
      purchaserAddress: model!.fullAddress,
      purchaserLat: model!.lat,
      purchaserLng: model!.lng,
      sellerID: sellerID,
      getOrderID: getOrderID,
    )));



  }




  @override
  Widget build(BuildContext context)
  {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const Padding(
          padding: EdgeInsets.all(10.0),
          child: Text(
              'Shipping Details:',
              style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold)
          ),
        ),
        const SizedBox(
          height: 6.0,
        ),
        Container(
          padding: const EdgeInsets.symmetric(horizontal: 90, vertical: 5),
          width: MediaQuery.of(context).size.width,
          child: Table(
            children: [
              TableRow(
                children: [
                  const Text(
                    "Name",
                    style: TextStyle(color: Colors.black),
                  ),
                  Text(model!.name!),
                ],
              ),
              TableRow(
                children: [
                  const Text(
                    "Phone Number",
                    style: TextStyle(color: Colors.black),
                  ),
                  Text(model!.phoneNumber!),
                ],
              ),
            ],
          ),
        ),
        const SizedBox(
          height: 20,
        ),
        Padding(
          padding: const EdgeInsets.all(10.0),
          child: Text(
            model!.fullAddress!,
            textAlign: TextAlign.justify,
          ),
        ),


        orderStatus == "ended"
            ? Container()
            : Padding(
          padding: const EdgeInsets.all(10.0),
          child: Center(
            child: InkWell(
              onTap: ()
              {
                UserLocation uLocation = UserLocation();
                uLocation!.getCurrentLocation();
                
                confirmedOrderShipment(context, orderID!, sellerID!, orderByUser!);
                //Navigator.push(context, MaterialPageRoute(builder: (context) => const HomeScreen()));
              },
              child: Container(
                decoration: const BoxDecoration(
                    gradient: LinearGradient(
                      colors: [
                        Colors.cyan,
                        Colors.amber,
                      ],
                      begin:  FractionalOffset(0.0, 0.0),
                      end:  FractionalOffset(1.0, 0.0),
                      stops: [0.0, 1.0],
                      tileMode: TileMode.clamp,
                    )
                ),
                width: MediaQuery.of(context).size.width - 40,
                height: 50,
                child: const Center(
                  child: Text(
                    "Confirm - To Deliver this Parcel",
                    style: TextStyle(color: Colors.white, fontSize: 15.0),
                  ),
                ),
              ),
            ),
          ),
        ),


        Padding(
          padding: const EdgeInsets.all(10.0),
          child: Center(
            child: InkWell(
              onTap: ()
              {
                Navigator.push(context, MaterialPageRoute(builder: (context) => const MySplashScreen()));
              },
              child: Container(
                decoration: const BoxDecoration(
                    gradient: LinearGradient(
                      colors: [
                        Colors.cyan,
                        Colors.amber,
                      ],
                      begin:  FractionalOffset(0.0, 0.0),
                      end:  FractionalOffset(1.0, 0.0),
                      stops: [0.0, 1.0],
                      tileMode: TileMode.clamp,
                    )
                ),
                width: MediaQuery.of(context).size.width - 40,
                height: 50,
                child: const Center(
                  child: Text(
                    "Go Back",
                    style: TextStyle(color: Colors.white, fontSize: 15.0),
                  ),
                ),
              ),
            ),
          ),
        ),  //Go Back Button

        SizedBox(height: 20,),
      ],
    );
  }
}
