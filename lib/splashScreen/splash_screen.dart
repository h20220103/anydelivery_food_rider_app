import 'dart:async';

import 'package:flutter/material.dart';

import '../authentication/auth_screen.dart';
import '../global/global.dart';
import '../mainScreens/homescreen.dart';

class MySplashScreen extends StatefulWidget {
  const MySplashScreen({Key? key}) : super(key: key);

  @override
  State<MySplashScreen> createState() => _MySplashScreenState();
}

class _MySplashScreenState extends State<MySplashScreen>
{

  startTimer()
  {
    // Displaying the start image for 10 seconds before sending the user to home or authenication screen

    Timer(const Duration(seconds: 10), () async {

      if(firebaseAuth.currentUser!=null){
        Navigator.push(context, MaterialPageRoute(builder: (c) => const HomeScreen())); // If already logged in then redirecting to Home Page after splash screen
      }
      else {
        Navigator.push(context, MaterialPageRoute(builder: (
            c) => const AuthScreen())); // Otherwise asking to login or register in the app
      }

    });
  }


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    startTimer();
  }
  @override
  Widget build(BuildContext context) {
    return Material(
      child: Container(
        color: Colors.white,
        child: Center(
          child: Column (
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.all(12.0),
                child: Image.asset("images/logo.png"),
              ),
              const SizedBox(height: 10,),
              const Padding(
                padding: EdgeInsets.all(18.0),
                child: Text(
                  "Riders Unite Online",
                   textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Colors.black54,
                    fontSize: 50,
                    fontFamily: "Signatra",
                    letterSpacing: 3,

                  ),
                ),

              ),
            ],
          ),
        ),
      ),
    );
  }
}
